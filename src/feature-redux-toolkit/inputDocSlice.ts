import {createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import type { PayloadAction } from '@reduxjs/toolkit'
import type { RootState } from './store';
import axios from 'axios';

export interface inputDocListState{
    inputDocListState :{
        docList: inputDoc[];
        paragraph: string[];
        loading: boolean;
        status: string;
    }
}
export interface inputDoc {
    docName: string;
    docId: string;
    page: string[];
}

const initialState:inputDocListState = {
    //BASE 문서 문단추가 
    inputDocListState:{
        docList: [
            {docId:'asdas',docName:'input 문서1 from redux RTK' , page:['sadf','asfgsf']},
            {docId:'asdasaaa',docName:'input 문서2 from redux RTK' , page:['sadf','asfgsf']},
        ],    
        paragraph:[],
        loading: false,
        status: 'wait',
    }    
}

//create the thunk TODO:
export const getDocs:any = createAsyncThunk(
    'sth/list',
    async (arg, {rejectWithValue}) =>{
        try {
            const { data } =  await axios.get(
                    'https://jsonplaceholder.typicode.com/posts'
                );
            return data;    
        } catch (error:any) {
            rejectWithValue(error.response.data);
            return error?.response;
        }
    }
)




export const DocListStateSlice = createSlice(
  {
    name: 'DocListState',
    initialState : initialState,
    reducers: {
        //TODO: remove
        getDocListState(state:inputDocListState,action:PayloadAction<inputDocListState>) {
            state.inputDocListState = {
                docList : [...state.inputDocListState.docList,...action.payload.inputDocListState.docList],
                loading : false,
                paragraph: [],
                status : 'succeeded',
            }
        },
        getPageListByDocId(state:inputDocListState,action:PayloadAction<inputDocListState>){
            state.inputDocListState = {
                docList: state.inputDocListState.docList,
                paragraph: [],
                status: 'succeeded',
                loading:false,
            }
        }
    }
  }  
);

// generate the action creators
export const { getDocListState } = DocListStateSlice.actions;

//selector 
export const getInputDocList = (state:RootState) => state.inputDocList.inputDocListState.docList;

// export reducers
export default DocListStateSlice.reducer;
