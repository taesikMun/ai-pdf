import {createSlice ,createAsyncThunk } from '@reduxjs/toolkit';
import type { RootState } from './store';
import axios from 'axios';

export interface projectDocState{
//     docList : Array<projectDoc>; 
//     status : string; // loading, failed, succeeded, idle
//     error : undefined;
//     message: string;

// test
// "userId": 1,
// "id": 6,
// "title": "dolorem eum magni eos aperiam quia",
// "body": 
    docList : Array<projectDoc>; 
    status : string; // loading, failed, succeeded, idle
    error : undefined;
    message: string;
 }

export interface projectDoc {
    // docName:string;

    userId : number;
    id: number;
    title: string;
    body: string;
}

const initialState : projectDocState = {
    docList: [], 
    status: 'idle', 
    error : undefined,
    message: '',

    //test

}

//create the thunk TODO:
export const fetchPost:any = createAsyncThunk(
    'post/list',
    async (arg, {rejectWithValue}) =>{
        try {
            const { data } =  await axios.get(
                    'https://jsonplaceholder.typicode.com/posts'
                );
            return data;    
        } catch (error:any) {
            rejectWithValue(error.response.data);
            return error?.response;
        }
    }
)



const prjDocSlice = createSlice(
  {
    name: 'prjDocs',
    initialState : initialState,
    reducers:{
        setStatus (state,action){
            console.log('setStatus from prjDocSlice');
            state.status = action.payload;
        } 
    },
    extraReducers (builder) {
        builder
            .addCase(fetchPost.pending, (state,action)=>{
                state.status = 'loading';
            })
            .addCase(fetchPost.fulfilled, (state,action)=>{
                state.status = 'succeeded';
                
                state.docList = action.payload; //TODO: check
            })
            .addCase(fetchPost.rejected, (state,action)=>{
                state.status = 'failed';
                state.message = action.payload.data; //TODO: check
            })
    }
  });

// generate the action creators
export const { setStatus } = prjDocSlice.actions;

//selector
export const prjDocSelector = (state:RootState) => state.prjDocs.docList;
export const getPostsStatus = (state:RootState) => state.prjDocs.status;
export const getPostsError = (state:RootState) => state.prjDocs.error;
export const getMessage = (state:RootState) => state.prjDocs.message;

// export reducers
export default prjDocSlice.reducer;
