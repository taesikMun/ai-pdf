import {createSlice } from '@reduxjs/toolkit';
import type { PayloadAction } from '@reduxjs/toolkit'
import type { RootState } from './store';
//======================================================
// Home에서 프로젝트 삭제하기 버튼 처리할 대상 ---  checkbox 선택 전역 상태
//======================================================
export interface rowData{
    selectedRows :Array<string>; //id Array
}
const initialState:rowData = {
    //checkBox로 선택된 rowData
    selectedRows: [],  
}

export const rowSlice = createSlice(
  {
    name: 'selectedRows',
    initialState : initialState,
    reducers: {
        addSelectedRow(state:rowData,action:PayloadAction<string>) {
            const strArr = [...state.selectedRows,action.payload];
            
            state.selectedRows= strArr.filter((i,p)=>strArr.indexOf(i)===p);
        },
        deleteSelectedRow(state:rowData,action:PayloadAction<string>) {
            
            state.selectedRows = state.selectedRows.filter(row => row!== action.payload);
        },
        
    }
  }  
);

// generate the action creators
export const { addSelectedRow, deleteSelectedRow } = rowSlice.actions;

//selector
export const rowSelector = (state:RootState) => state.selectedRows.selectedRows;

// export reducers
export default rowSlice.reducer;
