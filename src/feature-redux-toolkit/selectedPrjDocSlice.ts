import {createSlice } from '@reduxjs/toolkit';
import type { PayloadAction } from '@reduxjs/toolkit'
import type { RootState } from './store';
//======================================================
// 새문서 추가하기에서 나오는 모달에서 문서 선택전역 상태
//======================================================
export interface prjDoc {
    selectedPrjDocs :Array<string>; //id Array
}
const initialState:prjDoc = {
    //checkBox로 선택된 문서들
    selectedPrjDocs: [],  
}

export const selectedPrjDocSlice = createSlice(
  {
    name: 'selectedPrjDocs',
    initialState,
    reducers: {
        addSelectedDoc(state:prjDoc,action:PayloadAction<string>) {
            const strArr = [...state.selectedPrjDocs,action.payload];
            //중복 제거
            state.selectedPrjDocs= strArr.filter((i,p)=>strArr.indexOf(i)===p);
        },
        deleteSelectedDoc(state:prjDoc,action:PayloadAction<string>) {
            
            state.selectedPrjDocs = state.selectedPrjDocs.filter(row => row!== action.payload);
        },
        
    }
  }  
);

// generate the action creators
export const { addSelectedDoc, deleteSelectedDoc } = selectedPrjDocSlice.actions;

//selector
export const getPrjDocs = (state:RootState) => state.selectedPrjDocs;

// export reducers
export default selectedPrjDocSlice.reducer;
