import {createSlice } from '@reduxjs/toolkit';
import type { PayloadAction } from '@reduxjs/toolkit'
import type { RootState } from '../feature-redux-toolkit/store';

export interface fileObj{
    fileState :{
        docName: string;
        fileType: string;
        pageNumber: number | undefined; 
        paragraphNumber:number | undefined;
        matchRate: number | undefined;
        paragraph: string | undefined; 

        otherDocName:string | undefined;
        otherFileType:string |undefined;
        otherPageNumber:number| undefined;
        otherParagraphNumber:number| undefined;
        otherParagraph:string | undefined ;
    }
}
const initialState:fileObj = {
    //BASE 문서 파일유형
    fileState:{
        docName: '',
        fileType: '',
        pageNumber: undefined,
        paragraphNumber:undefined,
        paragraph:'',
        matchRate: undefined,
// INPUT 문서 파일유형
        otherDocName:'',
        otherFileType:'',
        otherPageNumber:undefined,
        otherParagraphNumber:undefined,
        otherParagraph:'',

    }    
}

export const fileSlice = createSlice(
  {
    name: 'fileState',
    initialState : initialState,
    reducers: {
        getFileState(state:fileObj,action:PayloadAction<fileObj>) {
            state.fileState = {
                docName:action.payload.fileState.docName,
                fileType:action.payload.fileState.fileType,
                pageNumber:action.payload.fileState.pageNumber,
                paragraphNumber:action.payload.fileState.paragraphNumber,
                paragraph:action.payload.fileState.paragraph,
                matchRate: action.payload.fileState.matchRate,
                otherDocName: action.payload.fileState.otherDocName,
                otherFileType: action.payload.fileState.otherFileType,
                otherPageNumber: action.payload.fileState.otherPageNumber,
                otherParagraphNumber: action.payload.fileState.otherParagraphNumber,
                otherParagraph:action.payload.fileState.otherParagraph,
            }
        }
    }
  }  
);

// generate the action creators
export const { getFileState } = fileSlice.actions;

//selector 
export const fileSelector = (state:RootState) => state.fileState;

// export reducers
export default fileSlice.reducer;
