import {createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import type { PayloadAction } from '@reduxjs/toolkit'
import type { RootState } from './store';
import axios from 'axios';

export interface DocListState{
    DocListState :{
        docList: doc[];
        paragraph: string[];
        loading: boolean;
        status: string;
    }
}
export interface doc {
    docName: string;
    docId: string;
    page: string[];
    paragraph: string[];
}

const initialState:DocListState = {
    //BASE 문서 문단추가 
    DocListState:{
        docList: [
            {docId:'asdas',docName:'문서1 from redux RTK' , page:['sadf','asfgsf'],paragraph:['paragraph']},
            {docId:'asdasaaa',docName:'문서2 from redux RTK' , page:['sadf','asfgsf'],paragraph:['paragraph']},
        ],    
        paragraph:[],
        loading: false,
        status: 'wait',
    }    
}

//create the thunk TODO: make this
export const getDocs:any = createAsyncThunk(
    'sth/list',
    async (arg, {rejectWithValue}) =>{
        try {
            const { data } =  await axios.get(
                    'https://jsonplaceholder.typicode.com/posts'
                );
            return data;    
        } catch (error:any) {
            rejectWithValue(error.response.data);
            return error?.response;
        }
    }
)




export const DocListStateSlice = createSlice(
  {
    name: 'DocListState',
    initialState,
    reducers: {
            //TODO: remove this
        getDocListState(state:DocListState,action:PayloadAction<DocListState>) {
            state.DocListState = {
                docList : [...state.DocListState.docList,...action.payload.DocListState.docList],
                loading : false,
                paragraph: [],
                status : 'succeeded',
            }
        },
        getPageListByDocId(state:DocListState,action:PayloadAction<DocListState>){
            state.DocListState = {
                docList: state.DocListState.docList,
                paragraph: [],
                status: 'succeeded',
                loading:false,
            }
        }
    }
  }  
);

// generate the action creators
export const { getDocListState } = DocListStateSlice.actions;

// selector
export const getBaseDocList = (state:RootState) => state.baseDocList.DocListState.docList;

// export reducers
export default DocListStateSlice.reducer;
