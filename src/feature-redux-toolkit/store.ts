import {configureStore} from  '@reduxjs/toolkit';
import fileReducer from './infoSlice';
import rowReducer from './selectedRowSlice';
import prjDocReducer from './projectDocsSlice';
import selectedPrjDocReducer from './selectedPrjDocSlice';
import baseDocReducer from './baseDocSlice';
import inputDocReducer from './inputDocSlice';

 const store =configureStore({
    reducer:{
        fileState:fileReducer,
        selectedRows: rowReducer,
        // Add the generated reducer as a specific top-level slice
        //TODO:
        selectedPrjDocs:selectedPrjDocReducer,
        prjDocs: prjDocReducer,
        baseDocList: baseDocReducer,
        inputDocList: inputDocReducer,
    },
    // Adding the api middleware enables caching, invalidation, polling,
    // and other useful features of `rtk-query`.
    // middleware: (getDefaultMiddleware) =>getDefaultMiddleware().concat(prjDocApi.middleware),
});

// optional, but required for refetchOnFocus/refetchOnReconnect behaviors
// see `setupListeners` docs - takes an optional callback as the 2nd arg for customization
// setupListeners(store.dispatch);

export default store;

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch