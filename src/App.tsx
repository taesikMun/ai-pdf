import {BrowserRouter as Router, Routes, Route} from 'react-router-dom';
import ErrorBoundary from './components/ErrorBoundary/ErrorBoundary';
import Header from './components/common/Header';
import PrjAfterCmpr from './pages/PrjAfterCmpr';
import ProjectDetail from './pages/ProjectDetail';
import DocUpload from "./pages/DocUpload/DocUpload";
import ReqAnalysis from "./pages/ReqAnalysis/ReqAnalysis";
import Dashboard from "./pages/Dashboard/Dashboard";
import SmartSearch from "./pages/SmartSearch/SmartSearch";
import Main from "./pages/Main/Main";

function App() {
  return (
      <Router>
        <ErrorBoundary>
          <Header/>
        </ErrorBoundary>
        <Routes>
          <Route path='/' element={
            <ErrorBoundary>
              <Main/>
            </ErrorBoundary>
          }/>
          <Route path='/projects/:projectName/' element={<ErrorBoundary><ProjectDetail/></ErrorBoundary>}/>
          <Route path='/projects/comparison' element={<ErrorBoundary><PrjAfterCmpr/></ErrorBoundary>}/>
        </Routes>
      </Router>
  );
}

export default App;
