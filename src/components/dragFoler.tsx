import React, { useEffect } from 'react'
import ReactDOM from "react-dom";
import { useDropzone } from "react-dropzone";

export default function DragFoler(): JSX.Element {
    const { acceptedFiles, getRootProps, getInputProps } = useDropzone();
    const files : any = acceptedFiles.map((file : any) => (
        <li key={file.path}>
          {file.path} - {file.size} bytes
        </li>
      )); 

    useEffect(() =>{
        console.log(acceptedFiles,'acceptedFiles');
        // console.log(getInputProps,'getInputProps');
    },[acceptedFiles,getRootProps,getInputProps]);  
    return (
        <div className="grid  place-content-center h-96 max-h-96">
            <div 
            {...getRootProps({ className: " bg-cyan-500 h-40 w-40 shadow-lg inline-block rounded" })}
            >
                <input className='' {...getInputProps()} />
                <p className='relative bg-fuchsia-300'>Drag 'n' drop some files here</p>
            </div>
            {/* <aside>
                <ul>{files}</ul>
            </aside> */}
        </div>
  )
}
