import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchPost, getPostsStatus, prjDocSelector, projectDoc, setStatus } from "../../feature-redux-toolkit/projectDocsSlice";
import { addSelectedDoc, deleteSelectedDoc, getPrjDocs } from "../../feature-redux-toolkit/selectedPrjDocSlice";
import DocRowOnDocAddModal from "./../row/DocRowOnDocAddModal";
import Spinner from "../common/Spinner";

interface docAddModalProps {
  docList?: projectDoc[];
}

export default function DocAddModal(props:docAddModalProps): JSX.Element {
  const [showModal, setShowModal] = useState(false);
  const dispatch = useDispatch();
  const status = useSelector(getPostsStatus);
  const post = useSelector(prjDocSelector);
  const selectedPrjDocList = useSelector(getPrjDocs);
  useEffect(()=>{
      console.log(selectedPrjDocList, "selectedPrjDocList");
  },[selectedPrjDocList]);
  

  useEffect(()=>{
    if(status === 'idle') {
      dispatch(fetchPost());
    }
  },[status,dispatch]);


  

  return (
    <>
      <button
        className="bg-pink-500 text-white active:bg-pink-600 font-bold uppercase text-sm px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150"
        type="button"
        onClick={() => setShowModal(prevState =>!prevState)}
      >
        새 문서 추가하기
      </button>
      {showModal ? (
        <>
          <div
            className="justify-center items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none"
          >
            <div className="relative w-auto my-6 mx-auto max-w-3xl">
              {/*content*/}
              <div className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none">
                {/*header*/}
                <div className="flex items-start justify-between p-5 border-b border-solid border-slate-200 rounded-t">
                  <h3 className="text-3xl font-semibold">
                    새 문서 추가
                  </h3>
                </div>
                {/*body*/}
                <div className="relative p-6 flex-auto">
                  <div className="h-[300px] overflow-y-scroll">

                    {
                      status === 'loading' && <Spinner/>
                    }
                    {
                      post && post.map((item,index)=>{
                        return <DocRowOnDocAddModal title={item.title} key={index} id={item.id.toString()}/>
                      })
                    }
                  </div>
                </div>
                {/*footer*/}
                <div className="flex items-center justify-end p-6 border-t border-solid border-slate-200 rounded-b">
                  <button
                    className="text-red-500 background-transparent font-bold uppercase px-6 py-2 text-sm outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150"
                    type="button"
                    onClick={() => setShowModal(false)}
                  >
                    Close
                  </button>
                  <button
                    className="bg-emerald-500 text-white active:bg-emerald-600 font-bold uppercase text-sm px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150"
                    type="button"
                    onClick={() => setShowModal(false)}
                  >
                    Save Changes
                  </button>
                </div>
              </div>
            </div>
          </div>
          <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
        </>
      ) : null}
    </>
  );
}


