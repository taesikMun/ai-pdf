import React, { useEffect, useState } from 'react';
import { FileUploader } from 'react-drag-drop-files';
import  '../css/drag.css';
import { useDropzone } from "react-dropzone"


export default function DragDropFile(): JSX.Element {
    // drag state
    const fileTypes = ["JPG", "PNG", "GIF", "DIR"];
    const [file, setFile] = useState([] as any[]);
    const handleChange = (file: any) => {
      setFile(prevState=> [...prevState,file]);
    };
    useEffect(()=>{
        console.log(file,'file');
    },[file]);

  return (
    <FileUploader 
    handleChange={handleChange} 
    name="" 
    types={fileTypes} 
    hoverTitle={'drop here'} />
  );
}
