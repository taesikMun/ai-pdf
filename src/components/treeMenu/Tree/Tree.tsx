import React, {FunctionComponent, useState} from "react";
import {AiFillCaretRight} from "react-icons/ai";
import {VscTriangleDown} from "react-icons/vsc";


type Props = {
    node: node,

}

interface node {
    key: string;
    label: string;
    children: {
        key: string,
        label: string,
        children: node[]
    }[]
}

const Tree: FunctionComponent<Props> = ({node}): React.ReactElement => {

    const [clickState, setClickState] = useState<boolean>(false);
    const [step, setStep] = useState<number>(2);

    const onClick = () => {
        setClickState(!clickState);
    }

    return (
        <div>
            <div className='ml-5 flex'>
                {
                    clickState
                        ? <VscTriangleDown onClick={onClick} className='mt-2 '/>
                        : <AiFillCaretRight onClick={onClick} className='mt-1 '/>
                }
                <div
                    onClick={onClick}
                    className='ml-5 cursor-pointer'>
                    {
                        node.label
                    }
                </div>


            </div>
            {
                clickState &&
                node.children.map((node:{
                    key: string,
                    label: string,
                    children: node[]
                }, index: number) => (
                    <div key={index}
                         className='ml-5 cursor-pointer'>
                        {node.label}
                    </div>
                ))


            }</div>

    );
}

export default Tree;
