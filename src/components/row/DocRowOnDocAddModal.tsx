import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { addSelectedDoc, deleteSelectedDoc, getPrjDocs } from '../../feature-redux-toolkit/selectedPrjDocSlice';

interface DocRowOnDocAddModalProps {
    id: string;
    //TODO: edit
    title:string;
}

export default function DocRowOnDocAddModal(props:DocRowOnDocAddModalProps) {
    const [checked,setChecked] = useState(false);
    const dispatch = useDispatch();
    

    


    const checkHandler = () => {
        setChecked(prevState => !prevState);
        if(!checked) {
          dispatch(addSelectedDoc(props.id));
        }else {
          dispatch(deleteSelectedDoc(props.id));
        };
    }
    return (
    <div className='hover:bg-gray-300 cursor-pointer'  >
       <input checked={checked} onChange={checkHandler} className='ml-1' type='checkbox' />
       <span onClick={checkHandler} className='ml-3'>{props.title}</span> 
    </div>
  )
}
