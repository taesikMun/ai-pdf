import Spinner from "../common/Spinner";

export interface IDocRowProps {
    no?: number;
    docName?: string;
    fileSize?: number;
    isInput?: boolean;
}

export default function DocRow(props:IDocRowProps): JSX.Element {
    
    return (
    <div className={`${props.isInput && 'mr-2'} hover:bg-slate-300 text-xs flex   border-t-2 border-b-2 w-full`}>
        <div className = 'w-[10%] text-center truncate'>
            {props.no}
        </div>
        <div className = 'w-[70%]  text-center truncate'>
            {props.docName}
        </div>
        <Spinner/>
        <div className = 'w-[20%] flex text-center'>
          <div className="w-[70%] truncate">
            {props.fileSize} 
          </div>
          <div className="w-[30%]">
            KB
          </div>
          <Spinner/>
        </div>
    </div>
  )
}
