import React, { useEffect, useState } from 'react'
import { useDispatch } from 'react-redux';
import { Link,  useNavigate } from 'react-router-dom';
import { addSelectedRow, deleteSelectedRow } from '../../feature-redux-toolkit/selectedRowSlice';
import Spinner from '../common/Spinner';

export interface rowProps {
    id:string;
    projectName?:string;
    savedDate?:string;
}

export default function Row(props:rowProps): JSX.Element {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [checked,setChecked] = useState(false);
  // useEffect(()=>{
  //   console.log(checked,'checked on Row component');
  // },[checked]);

  const checkHandler = () => {
    setChecked(prevState => !prevState);

    if(!checked) {
      dispatch(addSelectedRow(props.id));
    }else {
      dispatch(deleteSelectedRow(props.id));
    };
  }

  return (
    // <Link to ={`/projects/${props.id}`}>
      <div className=' mx-auto hover:bg-slate-100 border-b-2 flex w-[80%] justify-center place-items-center'>
        <div className='flex w-[10%]'>
          <div className='  w-[50%] text-center '>
            <span className='text-ellipsis overflow-hidden' >
              {props.id}
            </span>
          </div>
          <input checked={checked} onChange={checkHandler} type="checkbox"  className='default:ring-2 mt-1 mr-2' />
          <div className='w-[30%]'>
            <Spinner/>
          </div>
        </div> 
        
        <div onClick={()=> navigate(`/projects/${props.id}`)} className='cursor-pointer flex overflow-hidden w-[70%]  text-center '>
          <div className='flex w-[80%] '>
            <p className='text-ellipsis overflow-hidden'>
              {props.projectName}
            </p>
          </div>
          <div className='flex '>
            <Spinner/>
          </div>
        </div>
        <div className='flex w-[20%] text-center'>
            {props.savedDate?.substring(0,12)}
          <Spinner/>  
        </div>
      </div>
    // </Link>
  )
}
