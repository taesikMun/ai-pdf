import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { doc, getBaseDocList } from '../../feature-redux-toolkit/baseDocSlice';
import { getFileState} from '../../feature-redux-toolkit/infoSlice';
import { getInputDocList, inputDoc } from '../../feature-redux-toolkit/inputDocSlice';
import DropdownOptions from '../common/DropdownOptions';
export interface CompareRowProps {
    // BASE 문서
    docName: string; //문서명 
    pageNumber: number; // 페이지 번호
    paragraphNumber:number; 
    paragraph: string; //문단
    
    matchRate: number; // 일치율

    inputDocName?: string; // INPUT 문서명
    inputPageNumber?: number; // INPUT 문서 페이지 번호
    inputParagraphNumber?: number; //INPUT 문서 문단 번호
    inputParagraph?: string; // INPUT 문서 문단
    
    // 부모 컴포넌트인 PrjAfterCmpr의 상태값을 바꿔줄 함수를 props로 받아옴
    checked: string; //
    setChecked:(str:string)=>void;
    id : number; //클릭한 셀이 무엇인지 구별하기위한 id , checked === id 로 구별
}

export default function CompareRow(props:CompareRowProps): JSX.Element {
  const dispatch = useDispatch();
  const baseDocs = useSelector(getBaseDocList);
  const inputDocs = useSelector(getInputDocList);
  const [checked,setChecked] = useState(false);
  const onChangeHandler = () =>{
    setChecked(prevState => !prevState);
  }

  const [paragraphOption,setParagraphOption] = useState('');
  const [paragraphOptionInput,setParagraphOptionInput] = useState('');
  const onClick = ()=>{
      setParagraphOption('');
      return dispatch(getFileState({
        fileState :{
          docName: props.docName,
          fileType: 'pdf',
          pageNumber: props.pageNumber, 
          paragraphNumber:props.paragraphNumber,
          matchRate: props.matchRate,
          otherDocName:props.inputDocName,
          otherFileType:'pdf',
          otherPageNumber:props.inputPageNumber,
          otherParagraphNumber: props.inputParagraphNumber,
          paragraph:props.paragraph,
          otherParagraph:props.inputParagraph,
        }
  
      }));
    }
    const isEven= ()=>{
      return props.id % 2 === 0;
    }
   
    return (
      <>
      <div className='flex  text-center justify-between text-sm w-full '>
          <div className={`flex w-[48%] ${isEven() && 'bg-slate-200'}`}>
            <div className='text-xs h-6 w-[8%]  border-2'>
              <input  onChange={onChangeHandler} checked={checked} className='h-4 w-4 ' type="checkbox" name="" id="" />
            </div>
            <div onClick={onClick} className='cursor-pointer font-semibold text-xs h-6 p-1 w-[35%] truncate border-2'>
                {props.docName}
            </div>
            <div onClick={onClick} className='cursor-pointer font-semibold text-xs h-6 p-1 w-[8%] truncate  border-2 '>
              {props.pageNumber}
            </div>
            <div onClick={onClick} className='cursor-pointer font-semibold text-xs truncate h-6 p-1 w-[8%] border-2'>
              {props.paragraphNumber}
            </div>
            <div onClick={onClick} className='cursor-pointer  font-semibold text-xs truncate h-6 p-1 w-[33%] border-2'>
                {props.paragraph}
            </div>
            <div className='font-semibold text-xs h-6  w-[8%] border-2'>
              
              <DropdownOptions 
              head={<span>추가</span>} 
              options={
                [
                  {
                    label: '추가',
                  onClick: () => console.log('추가')
                  },
                  {
                    label: '변경',
                  onClick: () => console.log('변경')
                  },
                  {
                    label: '삭제',
                  onClick: () => console.log('삭제')
                  },
                ]
              }
             />


            </div>
          </div>

          <div className=' align-middle h-6  border-2 w-[4%] px-[2px] text-neutral-700 text-center font-semibold'>
            <div className="">{props.matchRate} %</div>
          </div>

          <div className='flex w-[48%] hover:bg-slate-400 '>
           
            <div onClick={onClick}  className='cursor-pointer text-xs h-6 p-1 truncate w-[43%]  border-2'>
              {props.inputDocName}
            </div>
            <div 
                onClick={onClick}
                className='cursor-pointer text-xs truncate h-6 p-1 w-[8%]  border-2'>
                {props.inputPageNumber}
            </div>
            <div onClick={onClick} className='cursor-pointer truncate text-xs h-6 p-1 w-[8%] border-2'>
              {props.inputParagraphNumber}
            </div>
            <div onClick={onClick} className='cursor-pointer truncate text-xs h-6 p-1 w-[33%] border-2'>
               {props.inputParagraph}     
            </div>
            <div  className='relative text-left  text-xs h-6  w-[8%] border-2'>
            <DropdownOptions 
              head={<span>추가</span>} 
              options={
                [
                  {
                    label: '추가',
                  onClick: () => console.log('추가')
                  },
                  {
                    label: '변경',
                  onClick: () => console.log('변경')
                  },
                  {
                    label: '삭제',
                  onClick: () => console.log('삭제')
                  },
                ]
              }
             />
            </div>
          </div>
          
        </div>
        {
          paragraphOption === '추가' && 
          <div className='w-full border-2 border-stone-700 bg-slate-400'>
          <div className='w-[50%] text-center bg-orange-900'>
            <button className='border-2 border-stone-700 hover:shadow-lg text-gray-200 bg-gradient-to-b from-emerald-400 to-green-700 rounded my-3 px-3' >
            ADD TO BASE DOCUMENT
            </button>
          </div>
          
          
          <div className='w-[50%] bg-amber-300 py-1'>
            <div className="flex">
              <div className='border-2 border-stone-700 w-[30%] mx-3 mt-3'>문서목록</div>
              <div className='border-2 border-stone-700 w-[30%] mx-3 mt-3'>페이지목록</div>
              <div className='border-2 border-stone-700 w-[30%] mx-3 mt-3'>문단</div>
            </div>
            <div className="flex">
              <div className='border-2  border-stone-700  w-[30%] mx-3 mt-3'>
                {/* <div className='cursor-pointer hover:bg-slate-200'>문서1 </div>
                <div className='cursor-pointer hover:bg-slate-200'>문서2 </div>
                <div className='cursor-pointer hover:bg-slate-200'>문서3 </div> */}
                {
                  baseDocs&& 
                  baseDocs.map((item:doc,index:number)=>{
                    return (
                      <div key={item.docId} className='cursor-pointer hover:bg-slate-200'>{item.docName}</div>
                    )
                  })
                }
              </div>
              <div className='border-2 border-stone-700  w-[30%] mx-3 mt-3'>
                {

                }
              </div>
              <div className='border-2 border-stone-700  w-[30%] mx-3 mt-3'>
                {

                }
              </div>
             
            </div>
            
          </div>
        </div>
        }
        {
          paragraphOptionInput === '추가' &&
          <div className=' w-full border-2 border-stone-700 bg-slate-400'>
          <div className='ml-[50%] w-[50%] text-center bg-orange-900'>
            <button className='border-2 border-stone-700 hover:shadow-lg text-gray-200 bg-gradient-to-b from-emerald-400 to-green-700 rounded my-3 px-3' >
            ADD TO INPUT DOCUMENT
            </button>
          </div>
          
          
          <div className='ml-[50%] w-[50%] bg-amber-300 py-1'>
            <div className="flex">
              <div className='border-2 border-stone-700 w-[30%] mx-3 mt-3'>문서목록</div>
              <div className='border-2 border-stone-700 w-[30%] mx-3 mt-3'>페이지목록</div>
              <div className='border-2 border-stone-700 w-[30%] mx-3 mt-3'>문단</div>
            </div>
            <div className="flex">
              <div className='border-2  border-stone-700  w-[30%] mx-3 mt-3'>
                {/* <div className='cursor-pointer hover:bg-slate-200'>문서1 </div>
                <div className='cursor-pointer hover:bg-slate-200'>문서2 </div>
                <div className='cursor-pointer hover:bg-slate-200'>문서3 </div> */}
                {
                  inputDocs&& 
                  inputDocs.map((item:inputDoc,index:number)=>{
                    return (
                      <div key={item.docId} className='cursor-pointer hover:bg-slate-200'>{item.docName}</div>
                    )
                  })
                }
              </div>
              <div className='border-2 border-stone-700  w-[30%] mx-3 mt-3'>
                {

                }
              </div>
              <div className='border-2 border-stone-700  w-[30%] mx-3 mt-3'>
                {

                }
              </div>
             
            </div>
            
          </div>
        </div>
        }
      </>
     
  )
}
