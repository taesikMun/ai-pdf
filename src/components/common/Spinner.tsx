import spinner from '../../assets/Spinner.svg';
export default function Spinner(): JSX.Element {
  return (
    <div>
        <div>
            <img src={spinner} className='h-5 rounded-sm mt-1' alt='loading'/>
        </div>
    </div>
  )
}
