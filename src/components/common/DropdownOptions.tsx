import { FC, ReactNode, useState } from "react";

interface Props {
  options: { label: string; onClick(): void }[];
  head: ReactNode;
}

const DropdownOptions: FC<Props> = ({ head, options }): JSX.Element => {
  const [showOptions, setShowOptions] = useState(false);

  return (
    <button 
    onBlur = {() => setShowOptions(false)}
    onMouseDown={()=>setShowOptions(!showOptions)} 
    className="relative">
        {head}
        {
            showOptions && <div className=" bg-slate-200 w-24 min-w-max absolute  mt-4  z-10 border-2  rounded  ">
            <ul className=''>
                {
                   options.map(({label, onClick},index)=>{
                    return <li key={label+index} className='text-xl  text-center h-10' onMouseDown={onClick}>{label}</li>
                   }) 
                }
            </ul>
          </div>
        }
      
    </button>
  );
};

export default DropdownOptions;
