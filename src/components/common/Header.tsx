import React, {useCallback, useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {useLocation, useNavigate} from "react-router-dom";

import img from './../../assets/logo.png';
import {FaUserCircle} from "react-icons/fa";
import {TbDoorExit, TbMinusVertical} from "react-icons/tb";
// export interface fileInfo {
//   docName?:string;
//   fileType?:string;
//   pageNumber?:number;
//   paragraphNumber?:number;
//   matchRate?:number;
// }
export default function Header(): JSX.Element {
  const location = useLocation();
  const navigate = useNavigate();
  const dispatch = useDispatch();


  const onClick = useCallback((e: any) => {
    navigate(e.target.id);
  }, []);

  const isDocUploadPage = () => location.pathname === '/doc-upload';
  const isExamplesPage = () => location.pathname === '/sample_videos';


  return (
      <div className=' sticky top-0  shadow-xl bg-white z-30 '>
        <div className='h-24 bg-gradient-to-r from-black to-gray-700  flex  items-center px-3  '>
          {/*고객사의 로고*/}
          <div></div>
          {/* search bar */}
          <div className={' text-black flex'}>
            {/*<div className={'w-[18%]'}></div>*/}
            <div className={' flex text-xs mt-3'}>
              <div
                  id='/doc-upload'
                  onClick={onClick}
                  className={`text-gray-400 font-bold mt-1 cursor-pointer px-3 `}>
                Remove Video Background
              </div>
              <div
                  id='/sample_videos'
                  onClick={onClick}
                  className={` font-bold mt-1 cursor-pointer px-3 text-gray-400`}>
                Examples
              </div>
              <div
                  id='/Pricing'
                  onClick={onClick}
                  className={` font-bold mt-1 cursor-pointer px-3 text-gray-400`}>
                Pricing
              </div>
              <div
                  id='/api'
                  onClick={onClick}
                  className={` font-bold mt-1 cursor-pointer px-3 text-gray-400`}>
                API
              </div>
              <div
                  id='/faq'
                  onClick={onClick}
                  className={` font-bold mt-1 cursor-pointer px-3 text-gray-400`}>
                FAQ
              </div>


            </div>


          </div>


        </div>


      </div>
  )
}

