import React, {useEffect, useState} from "react";

const Main: React.FC = (props): React.ReactElement => {

  const [selectedFile, setSelectedFile] = useState<File>();
  const [img, setImg] = useState<File>();
  const [fileURL, setFileURL] = useState('');


  function handleFileInputChange(e: any) {
    setSelectedFile(e.target.files[0]);
    const url = URL.createObjectURL(e.target.files[0]);
    setFileURL(prevState => url);
  }

  function handleImgInputChange(e: any) {
    setImg(e.target.files[0]);
    const url = URL.createObjectURL(e.target.files[0]);
    setFileURL(prevState => url);
  }


  return <>
    <div className={''}>
      <div className={'  flex justify-between w-full bg-black text-gray-400'}>

        <span className={'pt-6 pl-6 w-36'}>
          Remove Video
            Background
        </span>
        <div className={'flex-col px-6 pt-6'}>
          <label>
            <input type="file"
                   accept={'.mp4,.webm,.ogg,.ogv,.mov'}
                   title={'user'}
                   onChange={handleFileInputChange}
                   className={'text-gray-100  cursor-pointer rounded bg-sky-400  '}/>
          </label>
          <p className={'w-36'}>Supported formats: .mp4, .webm, .ogg, .ogv, .mov, .gif</p>
          <label>
            <input type="file"
                   accept={'.jpg,.jpeg,png,.bmp'}
                   onChange={handleFileInputChange}
                   className={'text-gray-100  cursor-pointer rounded bg-sky-400  '}/>
          </label>
          <p className={'w-36'}>Supported image formats: .jpg, bmp ,png</p>
        </div>
      </div>
      <div className={'h-screen bg-black'}>
        <video src={fileURL} controls>
          {/*<source src={fileURL} type="video/mp4"/>*/}
          {/*Your browser does not support the video tag.*/}
        </video>
      </div>

    </div>

  </>
      ;
}

export default Main;
