import React, {FunctionComponent, useCallback, useEffect, useState} from "react";
import {GrDocumentPdf} from "react-icons/gr";
import {AiOutlineProject} from "react-icons/ai";
import {BiCategoryAlt} from "react-icons/bi";
import {MdModelTraining} from "react-icons/md";

const DocUpload: FunctionComponent = (props): React.ReactElement => {


  const [tabState, setTabState] = useState<string>('');

  const onClick = useCallback((e: any) => {
    setTabState(e.target.id);
  }, []);
  const isDocType = () => tabState === 'doc';
  const isProject = () => tabState === 'prj';
  const isCategory = () => tabState === 'category';
  const isTrain = () => tabState === 'train';

  return (
      <div className=' '>
        <div className={'justify-center flex'}>
          <div>
            <input type="checkbox" name="" id=""/>
            <input type="checkbox" name="" id=""/>
          </div>
        </div>
        <div className='flex mt-5 mx-5 '>

          <div

              className={` px-2 mx-6 ${isDocType() && 'border-b-4 border-amber-900'}`}>
            <GrDocumentPdf className={'relative top-5'}/>
            <div
                onClick={onClick}
                id={'doc'}
                className={`ml-5  cursor-pointer  `}>
              Document Type
            </div>
          </div>
          <div

              className={`mx-6  ${isProject() && 'border-b-4 border-amber-900'}`}>
            <AiOutlineProject className='relative top-5'/>
            <div
                id={'prj'}
                onClick={onClick}
                className='ml-5 cursor-pointer'>Project
            </div>
          </div>
          <div
              className={`mx-6  ${isCategory() && 'border-b-4 border-amber-900'}`}>
            <BiCategoryAlt className='relative top-5'/>
            <div
                id={'category'}
                onClick={onClick}
                className='ml-5 cursor-pointer'>
              Category
            </div>
          </div>
          <div
              className={`mx-6 cursor-pointer ${isTrain() && 'border-b-4 border-amber-900'}`}>
            <MdModelTraining className='relative top-5'/>
            <div
                id={'train'}
                onClick={onClick}
                className='ml-5 cursor-pointer'>
              Train
            </div>
          </div>
        </div>
        {/* left Side about document type*/}
        <div className='ml-5 mt-5 w-[20%]  bg-gray-200'>
          <input placeholder='Add top-level document types' className='text-sm  my-3 border-2 w-[100%]' type="text"/>
          <div className='border-2 border-gray-400'>Document Type</div>
        </div>


      </div>
  );
}

export default DocUpload;
