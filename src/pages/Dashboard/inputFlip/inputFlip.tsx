import React, {FunctionComponent, useEffect, useState} from "react";
import {AiOutlineMinusSquare, AiOutlinePlusSquare} from "react-icons/ai";

type Props = {
    lines?: string[];
}
const InputFlip: FunctionComponent<Props> = ({lines}): React.ReactElement => {
    const [lineShow, setLineShow] = useState<boolean>(false);
    const [lineArr, setLineArr] = useState<string[] | undefined>(() => lines);
    const onClick = () => {
        setLineShow(!lineShow);
    }

    useEffect(() => {
        console.log(lineArr, 'lineArr');
    }, [lineArr]);

    return <>
        {
            lineShow && lines
                ? <AiOutlineMinusSquare id={'a'}
                                        className={'absolute'}
                                        style={{cursor: 'pointer'}}
                                        onClick={onClick}/>
                : <AiOutlinePlusSquare
                    className={'absolute '}
                    style={{cursor: 'pointer'}}
                    onClick={onClick}/>
        }
        <input type="text" className={'border-2 w-12'}/>
        {
            lineShow && lines &&
            lines.map((el, index) => {
                return <input
                    key={index}
                    type="text"
                    id={index.toString()}
                    className={'border-2 ml-3 w-3'}
                    value={el}
                    onChange={(e) => {
                        lineArr?.splice(index, 1, e.target.value);
                        setLineArr(lineArr);
                    }}
                    style={{borderWidth: '2px', marginLeft: '0.75rem'}}/>
            })
        }

    </>;
}

export default InputFlip;
