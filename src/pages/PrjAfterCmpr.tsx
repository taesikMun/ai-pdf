import { useState } from "react";
import CompareRow from "../components/row/CompareRow";

export default function PrjAfterCmpr(){
  
  const [selectedRow, setSelectedRow] = useState('');
  const setSelectedRowState = (str:string) =>{
    setSelectedRow(str);
  }
  
  return (
    <div className="">
        {/* 셀 클릭 시 정보 노출할 영역 */}
        <div className='  w-full mt-2 px-4 top-60 '>
          <div className='flex justify-between'>
            <div className='bg-emerald-200 text-center w-[48%]'>BASE 문서</div>
            <div className='bg-sky-700 px-[2px] text-sm w-[4%] text-neutral-100 text-center font-semibold'>일치율</div>
            <div className='bg-lime-700 text-gray-50 text-center w-[48%]'>INPUT 문서</div>
          </div>
        {/* column */}
        <div className='flex  text-center justify-between text-sm w-full '>
          <div className='flex w-[48%]'>
            <div className='text-xs h-14 pt-1  w-[8%]  bg-slate-200 font-semibold border-2 border-b-zinc-600 border-r-gray-400'>
              확인여부
            </div>
            <div className='text-xs h-14 pt-3 w-[35%]  bg-slate-200 font-semibold border-2 border-b-zinc-600 border-r-gray-400'>
              문서명
            </div>
            <div className='  text-center text-xs h-14 pt-1 w-[8%]  bg-slate-200 font-semibold border-2 border-b-zinc-600 border-r-gray-400 '>
              페이지 번호
            </div>
            <div className='text-xs h-14 pt-1 w-[8%] bg-slate-200 font-semibold border-2 border-b-zinc-600 border-r-gray-400'>
              문단번호
            </div>
            <div className='text-xs  h-14 pt-3 w-[33%] bg-slate-200 font-semibold border-2 border-b-zinc-600 border-r-gray-400'>문단</div>
            <div className='text-clip text-xs h-14 pt-3 w-[8%] bg-slate-200 font-semibold border-2 border-b-zinc-600 border-r-gray-400'>문단변경</div>
          </div>


          <div className='flex w-[48%]'>
            <div className='text-xs h-14 pt-3 w-[43%]  bg-slate-200 font-semibold border-2 border-b-zinc-600 border-r-gray-400'>
              문서명
            </div>
            <div className='text-xs h-14 pt-1 w-[8%]  bg-slate-200 font-semibold border-2 border-b-zinc-600 border-r-gray-400 '>
            페이지번호
            </div>
            <div className='text-xs h-14 pt-1 w-[8%] bg-slate-200 font-semibold border-2 border-b-zinc-600 border-r-gray-400'>문단번호</div>
            <div className='text-xs h-14 pt-3 w-[33%] bg-slate-200 font-semibold border-2 border-b-zinc-600 border-r-gray-400'>문단</div>
            <div className='text-xs h-14 pt-3 w-[8%] bg-slate-200 font-semibold border-2 border-b-zinc-600 border-r-gray-400'>문단변경</div>
          </div>
          
        </div>   
              {/* row */}
              <CompareRow id={1}
                    docName="문서1_지능성_디지털맵구성방안" 
                    matchRate={20} pageNumber={111} paragraph='Everything you never wanted to know about the future of talking about the future.Everything you never wanted to know about the future of talking about the future.Everything you never wanted to know about the future of talking about the future.Everything you never wanted to know about the future of talking about the future.Everything you never wanted to know about the future of talking about the future.Everything you never wanted to know about the future of talking about the future.Everything you never wanted to know about the future of talking about the future.Everything you never wanted to know about the future of talking about the future.Everything you never wanted to know about the future of talking about the future.'
                    paragraphNumber={2} inputDocName='문서4_METAVERSE_활용방안문서4_METAVERSE_활용방안문서4_METAVERSE_활용방안문서4_METAVERSE_활용방안'
                    inputPageNumber={31} inputParagraph={'The video then cuts to the concert, where the woman appears in an Avengers-style hologram. Shes able to make eye contact with her friend who is ph'}
                    checked={selectedRow}
                    setChecked={setSelectedRowState}
                    inputParagraphNumber={123} />
              <CompareRow id={2} docName="문서1_지능성_디지털맵구성방안" 
                    matchRate={20} pageNumber={111} paragraph='Everything you never wanted to know about the future of talking about the future.Everything you never wanted to know about the future of talking about the future.Everything you never wanted to know about the future of talking about the future.Everything you never wanted to know about the future of talking about the future.Everything you never wanted to know about the future of talking about the future.Everything you never wanted to know about the future of talking about the future.Everything you never wanted to know about the future of talking about the future.Everything you never wanted to know about the future of talking about the future.Everything you never wanted to know about the future of talking about the future.'
                    paragraphNumber={2} inputDocName='문서4_METAVERSE_활용방안문서4_METAVERSE_활용방안문서4_METAVERSE_활용방안문서4_METAVERSE_활용방안'
                    inputPageNumber={31} inputParagraph={'The video then cuts to the concert, where the woman appears in an Avengers-style hologram. Shes able to make eye contact with her friend who is ph'}
                    checked={selectedRow}
                    setChecked={setSelectedRowState}
                    inputParagraphNumber={123} />
        
      </div>
    </div>
      
      
  )
}
