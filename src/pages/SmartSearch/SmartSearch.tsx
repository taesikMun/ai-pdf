import React, {useEffect, useState} from "react";
import {GrDocumentPdf} from "react-icons/gr";
import {TiMediaRecord, TiMediaRecordOutline} from "react-icons/ti";

const SmartSearch: React.FC = (props): React.ReactElement => {
  // const handleSubmit = (event: any) => {
  //   event.preventDefault();
  //   // Perform submission logic here
  // }

  // dual / single
  const [option, setOption] = useState('');

  // 듀얼 운영 /미운영
  const [dualOperOpt, setDualOperOpt] = useState('');
  // 1호기
  const [serverAOperOpt, setServerAOperOpt] = useState('');

  // 2호기
  const [serverBOperOpt, setServerBOperOpt] = useState('');

  const [dualStartTime, setDualStartTime] = useState('18:24');
  const [dualEndTime, setDualEndTime] = useState('19:00');
  const [dualOperStartDate, setDualOperStartDate] = useState('2023-01-10');
  const [dualOperEndDate, setDualOperEndDate] = useState('');


  const [headQuarter, setHeadQuater] = useState([
    {
      isOperate: true,
      code: 1,
      name: '본원',
    },
    {
      isOperate: false,
      code: 2,
      name: '서울',
    },
    {
      isOperate: true,
      code: 3,
      name: '대전',
    },
    {
      isOperate: false,
      code: 4,
      name: '대구',
    },
  ]);
  const [hospitalArr, setHospitalArr] = useState([
    {
      isOperate: true,
      code: 1,
      name: '종합병원',
    },
    {
      isOperate: false,
      code: 2,
      name: '대학병원',
    },
    {
      isOperate: true,
      code: 3,
      name: '중형병원',
    },
    {
      isOperate: false,
      code: 4,
      name: '의원',
    },
  ]);


  const [serverAHeadQ, setServerAHeadQ] = useState(
      [
        {
          isOperate: true,
          code: 1,
          name: '본원',
        },
        {
          isOperate: false,
          code: 2,
          name: '서울',
        },
        {
          isOperate: true,
          code: 3,
          name: '대전',
        },
        {
          isOperate: false,
          code: 4,
          name: '대구',
        },
      ]
  );

  const [serverAHospitalArr, setServerAHospitalArr] = useState(
      [
        {
          isOperate: true,
          code: 1,
          name: '종합병원',
        },
        {
          isOperate: false,
          code: 2,
          name: '대학병원',
        },
        {
          isOperate: true,
          code: 3,
          name: '중형병원',
        },
        {
          isOperate: false,
          code: 4,
          name: '의원',
        },
      ]
  )
  const [serverAStartTime, setServerAStartTime] = useState('');
  const [serverAEndTime, setServerAEndTime] = useState('');

  const [serverAStartDate, setServerAStartDate] = useState('');
  const [serverAEndDate, setServerAEndDate] = useState('');


  const [serverBHeadQ, setServerBHeadQ] = useState(
      [
        {
          isOperate: true,
          code: 1,
          name: '본원',
        },
        {
          isOperate: false,
          code: 2,
          name: '서울',
        },
        {
          isOperate: true,
          code: 3,
          name: '대전',
        },
        {
          isOperate: false,
          code: 4,
          name: '대구',
        },
      ]
  );

  const [serverBHospitalArr, setServerBHospitalArr] = useState(
      [
        {
          isOperate: true,
          code: 1,
          name: '종합병원',
        },
        {
          isOperate: false,
          code: 2,
          name: '대학병원',
        },
        {
          isOperate: true,
          code: 3,
          name: '중형병원',
        },
        {
          isOperate: false,
          code: 4,
          name: '의원',
        },
      ]
  )

  const [serverBStartTime, setServerBStartTime] = useState('');
  const [serverBEndTime, setServerBEndTime] = useState('');

  const [serverBStartDate, setServerBStartDate] = useState('');
  const [serverBEndDate, setServerBEndDate] = useState('');

  useEffect(() => {
    console.log(option, 'option')
  }, [option]);
  useEffect(() => {
    console.log(dualOperOpt, 'dualOperOpt')
  }, [dualOperOpt]);
  useEffect(() => {
    console.log(dualStartTime, 'dualStartTime')
  }, [dualStartTime]);
  useEffect(() => {
    console.log(dualOperStartDate, 'dualOperStartDate')
  }, [dualOperStartDate]);

  return <div>
    <div
        style={{
          height: '9rem',
          fontSize: '7rem',
          width: '100%',
          display: 'flex',
          backgroundColor: 'rgb(59 130 246)',
          color: 'rgb(255 255 255)',
        }}
    >
      Batch Admin
    </div>
    <div style={{display: 'flex'}}>
      <div style={{
        fontSize: '7rem',
      }}>
        dual
      </div>
      <input type="radio" name='a'
             style={{
               marginTop: '3rem',
               height: '3rem',
               width: '3rem',
             }}
             onChange={(e) => {
               setOption(e.target.value);
             }
             }
             value={'dual'}/>
      <div style={{
        fontSize: '7rem',
      }}>
        single
      </div>
      <input type="radio" name={'a'}
             onChange={(e) => {
               setOption(e.target.value);
             }
             }
             style={{
               marginTop: '3rem',
               height: '3rem',
               width: '3rem',
             }}
             value={'single'}/>

    </div>
    {
        option === 'dual' && (
            <div>
              <div style={{
                display: 'flex',
              }}>
                <div style={{
                  fontSize: '7rem',
                }}>
                  운영
                </div>
                <input type="radio" name='b'
                       style={{
                         marginTop: '3rem',
                         height: '3rem',
                         width: '3rem',
                       }}
                       onChange={(e) => {
                         setDualOperOpt(e.target.value);
                       }
                       }
                       value={'operate'}/>
                <div className={'text-lg'}>
                  미운영
                </div>
                <input type="radio" name={'b'}
                       onChange={(e) => {
                         setDualOperOpt(e.target.value);
                       }
                       }
                       style={{
                         fontSize: '7rem',
                       }}
                       value={'stop'}/>


              </div>
              <div
                  style={{
                    display: 'flex',
                    height: '6rem',
                    backgroundColor: 'rgb(59 130 246)',
                    color: 'rgb(255 255 255)',
                    fontSize: '4rem',
                  }}
              >
                <div>Branch/Headquarter</div>
              </div>
              <div
                  style={{
                    display: 'flex',
                    marginTop: '1rem',
                  }}
              >
                <div
                    style={{
                      width: '300px',
                      fontSize: '4rem',
                      height: '6rem',
                      backgroundColor: 'rgb(37 99 235)',
                      color: 'rgb(255 255 255)',
                    }}
                >운영여부
                </div>
                <div
                    style={{
                      width: '300px',
                      fontSize: '4rem',
                      height: '6rem',
                      backgroundColor: 'rgb(37 99 235)',
                      color: 'rgb(255 255 255)',
                    }}
                >지원코드
                </div>
                <div
                    style={{
                      width: '300px',
                      fontSize: '4rem',
                      height: '6rem',
                      backgroundColor: 'rgb(37 99 235)',
                      color: 'rgb(255 255 255)',
                    }}

                >지원명
                </div>
              </div>

              <div>
                {
                    headQuarter && headQuarter.map((el) => {
                      return <div style={{display: 'flex'}}>
                        <div
                            style={{
                              width: '300px',
                              fontSize: '4rem',
                              height: '6rem',
                            }}
                        >
                          {
                            el.isOperate
                                ? <TiMediaRecord/>
                                : <TiMediaRecordOutline/>
                          }

                        </div>
                        <div style={{
                          width: '300px',
                          fontSize: '4rem',
                          height: '6rem',
                        }}>{el.code}</div>
                        <div style={{
                          width: '300px',
                          fontSize: '4rem',
                          height: '6rem',
                        }}>{el.name}</div>
                      </div>
                    })
                }
                <div className={'flex h-24 bg-blue-500 text-white text-md'}>
                  <div>Classification of hospital types
                  </div>
                </div>
                <div className={'flex mt-4'}>
                  <div style={{
                    width: '300px',
                    fontSize: '4rem',
                    height: '6rem',
                    backgroundColor: 'rgb(37 99 235)',
                    color: 'rgb(255 255 255)',
                  }}>운영여부
                  </div>
                  <div style={{
                    width: '300px',
                    fontSize: '4rem',
                    height: '6rem',
                    backgroundColor: 'rgb(37 99 235)',
                    color: 'rgb(255 255 255)',
                  }}>지원코드
                  </div>
                  <div style={{
                    width: '300px',
                    fontSize: '4rem',
                    height: '6rem',
                    backgroundColor: 'rgb(37 99 235)',
                    color: 'rgb(255 255 255)',
                  }}>지원명
                  </div>
                </div>
                {
                    hospitalArr && hospitalArr.map((el) => {
                      return <div style={{
                        display: 'flex',
                      }}>
                        <div
                            style={{
                              width: '300px',
                              fontSize: '4rem',
                              height: '6rem',
                            }}
                        >
                          {
                            el.isOperate
                                ? <TiMediaRecord/>
                                : <TiMediaRecordOutline/>
                          }

                        </div>
                        <div style={{
                          width: '300px',
                          fontSize: '4rem',
                          height: '6rem',
                        }}>{el.code}</div>
                        <div style={{
                          width: '300px',
                          fontSize: '4rem',
                          height: '6rem',
                        }}>{el.name}</div>
                      </div>
                    })
                }
              </div>

              <div
                  style={{
                    display: 'flex',
                    marginTop: '1rem',
                  }}
              >
                <div
                    style={{
                      width: '100%',
                      fontSize: '4rem',
                      height: '6rem',
                      backgroundColor: 'rgb(37 99 235)',
                      color: 'rgb(255 255 255)'
                    }}

                >Operation time
                </div>
              </div>

              <div style={{
                fontSize: '4rem',
              }}>
                <div
                    style={{
                      marginTop: '0.75rem',
                      display: 'flex',
                      backgroundColor: 'rgb(37 99 235)',
                      color: 'rgb(255 255 255)'
                    }}

                >시작시간
                </div>
                <div style={{
                  display: 'flex',
                }}>
                  <input type="time" value={dualStartTime} onChange={e => setDualStartTime(e.target.value)}/>
                </div>
                <div style={{
                  marginTop: '0.75rem',
                  display: 'flex',
                  backgroundColor: 'rgb(37 99 235)',
                  color: 'rgb(255 255 255)'
                }}>종료시간
                </div>
                <div style={{
                  display: 'flex',
                }}>
                  <input type="time" value={dualEndTime} onChange={e => setDualEndTime(e.target.value)}/>
                </div>

                <div style={{
                  marginTop: '0.75rem',
                  display: 'flex',
                  backgroundColor: 'rgb(37 99 235)',
                  color: 'rgb(255 255 255)'
                }}> 시작일자
                </div>
                <input type="date" value={dualOperStartDate} onChange={e => setDualOperStartDate(e.target.value)}/>
                <div style={{
                  marginTop: '0.75rem',
                  display: 'flex',
                  backgroundColor: 'rgb(37 99 235)',
                  color: 'rgb(255 255 255)'
                }}>끝일자
                </div>
                <input type="date" value={dualOperEndDate} onChange={e => setDualOperEndDate(e.target.value)}/>
              </div>
            </div>

        )
    }
    {
        option === 'single' && (
            <div>

              <div style={{
                display: 'flex',
              }}>
                <div
                    style={{
                      width: '15rem',
                      backgroundColor: 'rgb(37 99 235)',
                      color: 'rgb(255 255 255)',
                      fontSize: '4rem',
                    }}
                > 1호기
                </div>
                <div style={{
                  fontSize: '7rem',
                }}>
                  운영
                </div>
                <input type="radio" name='b'
                       style={{
                         marginTop: '3rem',
                         width: '3rem',
                         height: '3rem',
                       }}
                       onChange={(e) => {
                         setServerAOperOpt(e.target.value);
                       }
                       }
                       value={'operate'}/>
                <div style={{
                  fontSize: '7rem',
                }}>
                  미운영
                </div>
                <input type="radio" name={'b'}
                       onChange={(e) => {
                         setServerAOperOpt(e.target.value);
                       }
                       }

                       style={{
                         marginTop: '3rem',
                         height: '3rem',
                         width: '3rem',
                       }}
                       value={'stop'}/>


              </div>
              <div
                  style={{
                    display: 'flex',
                    height: '6rem',
                    backgroundColor: 'rgb(59 130 246)',
                    color: 'rgb(255 255 255)',
                    fontSize: '4rem',
                  }}
              >
                <div>Branch/Headquarter</div>
              </div>
              <div
                  style={{
                    display: 'flex',
                    marginTop: '1rem',
                  }}
              >
                <div
                    className={'w-[300px] text-md h-24 bg-blue-600 text-white'}
                    style={{
                      width: '300px',
                      fontSize: '4rem',
                      height: '6rem',
                      backgroundColor: 'rgb(37 99 235)',
                      color: 'rgb(255 255 255)',
                    }}
                >운영여부
                </div>
                <div style={{
                  width: '300px',
                  fontSize: '4rem',
                  height: '6rem',
                  backgroundColor: 'rgb(37 99 235)',
                  color: 'rgb(255 255 255)',
                }}>지원코드
                </div>
                <div style={{
                  width: '300px',
                  fontSize: '4rem',
                  height: '6rem',
                  backgroundColor: 'rgb(37 99 235)',
                  color: 'rgb(255 255 255)',
                }}>지원명
                </div>
              </div>

              <div>
                {
                    serverAHeadQ && serverAHeadQ.map((el) => {
                      return <div style={{
                        display: 'flex',
                      }}>
                        <div
                            style={{
                              width: '300px',
                              fontSize: '4rem',
                              height: '6rem',
                            }}
                        >
                          {
                            el.isOperate
                                ? <TiMediaRecord/>
                                : <TiMediaRecordOutline/>
                          }

                        </div>
                        <div style={{
                          width: '300px',
                          fontSize: '4rem',
                          height: '6rem',
                        }}>{el.code}</div>
                        <div style={{
                          width: '300px',
                          fontSize: '4rem',
                          height: '6rem',
                        }}>{el.name}</div>
                      </div>
                    })
                }
                <div
                    style={{
                      display: 'flex',
                      height: '6rem',
                      backgroundColor: 'rgb(59 130 246)',
                      color: 'rgb(255 255 255)',
                      fontSize: '4rem',
                    }}>
                  <div>Classification of hospital types
                  </div>
                </div>
                <div
                    style={{
                      marginTop: '1rem',
                      display: 'flex',
                    }}
                >
                  <div style={{
                    width: '300px',
                    fontSize: '4rem',
                    height: '6rem',
                    backgroundColor: 'rgb(37 99 235)',
                    color: 'rgb(255 255 255)',
                  }}>운영여부
                  </div>
                  <div style={{
                    width: '300px',
                    fontSize: '4rem',
                    height: '6rem',
                    backgroundColor: 'rgb(37 99 235)',
                    color: 'rgb(255 255 255)',
                  }}>지원코드
                  </div>
                  <div style={{
                    width: '300px',
                    fontSize: '4rem',
                    height: '6rem',
                    backgroundColor: 'rgb(37 99 235)',
                    color: 'rgb(255 255 255)',
                  }}>지원명
                  </div>
                </div>
                {
                    serverAHospitalArr && serverAHospitalArr.map((el) => {
                      return <div style={{
                        display: 'flex',
                      }}>
                        <div
                            style={{
                              width: '300px',
                              fontSize: '4rem',
                              height: '6rem',
                            }}
                        >
                          {
                            el.isOperate
                                ? <TiMediaRecord/>
                                : <TiMediaRecordOutline/>
                          }

                        </div>
                        <div style={{
                          width: '300px',
                          fontSize: '4rem',
                          height: '6rem',
                        }}>{el.code}</div>
                        <div style={{
                          width: '300px',
                          fontSize: '4rem',
                          height: '6rem',
                        }}>{el.name}</div>
                      </div>
                    })
                }
              </div>

              <div
                  style={{
                    display: 'flex',
                    marginTop: '1rem',
                  }}
              >
                <div style={{
                  width: '100%',
                  fontSize: '4rem',
                  height: '6rem',
                  backgroundColor: 'rgb(37 99 235)',
                  color: 'rgb(255 255 255)'
                }}>Operation time
                </div>
              </div>

              <div style={{
                fontSize: '4rem',
              }}>
                <div style={{
                  marginTop: '0.75rem',
                  display: 'flex',
                  backgroundColor: 'rgb(37 99 235)',
                  color: 'rgb(255 255 255)'
                }}
                >시작시간
                </div>
                <div style={{
                  display: 'flex',
                }}>
                  <input type="time" value={serverAStartTime} onChange={e => setServerAStartTime(e.target.value)}/>
                </div>
                <div style={{
                  marginTop: '0.75rem',
                  display: 'flex',
                  backgroundColor: 'rgb(37 99 235)',
                  color: 'rgb(255 255 255)'
                }}>종료시간
                </div>
                <div style={{
                  display: 'flex',
                }}>
                  <input type="time" value={serverAEndTime} onChange={e => setServerAEndTime(e.target.value)}/>
                </div>

                <div style={{
                  marginTop: '0.75rem',
                  display: 'flex',
                  backgroundColor: 'rgb(37 99 235)',
                  color: 'rgb(255 255 255)'
                }}> 시작일자
                </div>
                <input type="date" value={serverAStartDate} onChange={e => setServerAStartDate(e.target.value)}/>
                <div style={{
                  marginTop: '0.75rem',
                  display: 'flex',
                  backgroundColor: 'rgb(37 99 235)',
                  color: 'rgb(255 255 255)'
                }}>끝일자
                </div>
                <input type="date" value={serverAEndDate} onChange={e => setServerAEndDate(e.target.value)}/>
              </div>


              <div style={{
                display: 'flex',
              }}>
                <div style={{
                  width: '15rem',
                  backgroundColor: 'rgb(37 99 235)',
                  color: 'rgb(255 255 255)',
                  fontSize: '4rem',
                }}> 2호기
                </div>
                <div style={{
                  fontSize: '7rem',
                }}>
                  운영
                </div>
                <input type="radio" name='c'
                       className={'mt-12 h-12 w-12'}
                       onChange={(e) => {
                         setServerBOperOpt(e.target.value);
                       }
                       }
                       value={'operate'}/>
                <div style={{
                  fontSize: '7rem',
                }}>
                  미운영
                </div>
                <input type="radio" name={'c'}
                       onChange={(e) => {
                         setServerBOperOpt(e.target.value);
                       }
                       }
                       value={'stop'}/>


              </div>
              <div style={{
                display: 'flex',
                height: '6rem',
                backgroundColor: 'rgb(59 130 246)',
                color: 'rgb(255 255 255)',
                fontSize: '4rem',
              }}>
                <div>Branch/Headquarter</div>
              </div>
              <div style={{
                marginTop: '1rem',
                display: 'flex',
              }}>
                <div style={{
                  width: '300px',
                  fontSize: '4rem',
                  height: '6rem',
                  backgroundColor: 'rgb(37 99 235)',
                  color: 'rgb(255 255 255)',
                }}>운영여부
                </div>
                <div style={{
                  width: '300px',
                  fontSize: '4rem',
                  height: '6rem',
                  backgroundColor: 'rgb(37 99 235)',
                  color: 'rgb(255 255 255)',
                }}>지원코드
                </div>
                <div style={{
                  width: '300px',
                  fontSize: '4rem',
                  height: '6rem',
                  backgroundColor: 'rgb(37 99 235)',
                  color: 'rgb(255 255 255)',
                }}>지원명
                </div>
              </div>

              <div>
                {
                    serverBHeadQ && serverBHeadQ.map((el) => {
                      return <div style={{
                        display: 'flex',
                      }}>
                        <div style={{
                          width: '300px',
                          fontSize: '4rem',
                          height: '6rem',
                        }}>
                          {
                            el.isOperate
                                ? <TiMediaRecord/>
                                : <TiMediaRecordOutline/>
                          }

                        </div>
                        <div style={{
                          width: '300px',
                          fontSize: '4rem',
                          height: '6rem',
                        }}>{el.code}</div>
                        <div style={{
                          width: '300px',
                          fontSize: '4rem',
                          height: '6rem',
                        }}>{el.name}</div>
                      </div>
                    })
                }
                <div style={{
                  display: 'flex',
                  height: '6rem',
                  backgroundColor: 'rgb(59 130 246)',
                  color: 'rgb(255 255 255)',
                  fontSize: '4rem',
                }}>
                  <div>Classification of hospital types
                  </div>
                </div>
                <div style={{
                  marginTop: '1rem',
                  display: 'flex',
                }}>
                  <div style={{
                    width: '300px',
                    fontSize: '4rem',
                    height: '6rem',
                    backgroundColor: 'rgb(37 99 235)',
                    color: 'rgb(255 255 255)',
                  }}>운영여부
                  </div>
                  <div style={{
                    width: '300px',
                    fontSize: '4rem',
                    height: '6rem',
                    backgroundColor: 'rgb(37 99 235)',
                    color: 'rgb(255 255 255)',
                  }}>지원코드
                  </div>
                  <div style={{
                    width: '300px',
                    fontSize: '4rem',
                    height: '6rem',
                    backgroundColor: 'rgb(37 99 235)',
                    color: 'rgb(255 255 255)',
                  }}>지원명
                  </div>
                </div>
                {
                    serverBHospitalArr && serverBHospitalArr.map((el) => {
                      return <div style={{
                        display: 'flex',
                      }}>
                        <div style={{
                          width: '300px',
                          fontSize: '4rem',
                          height: '6rem',
                        }}>
                          {
                            el.isOperate
                                ? <TiMediaRecord/>
                                : <TiMediaRecordOutline/>
                          }

                        </div>
                        <div style={{
                          width: '300px',
                          fontSize: '4rem',
                          height: '6rem',
                        }}>{el.code}</div>
                        <div style={{
                          width: '300px',
                          fontSize: '4rem',
                          height: '6rem',
                        }}>{el.name}</div>
                      </div>
                    })
                }
              </div>

              <div style={{
                display: 'flex',
                marginTop: '1rem',
              }}>
                <div style={{
                  width: '100%',
                  fontSize: '4rem',
                  height: '6rem',
                  backgroundColor: 'rgb(37 99 235)',
                  color: 'rgb(255 255 255)'
                }}>Operation time
                </div>
              </div>

              <div style={{
                fontSize: '4rem',
              }}>
                <div style={{
                  marginTop: '0.75rem',
                  display: 'flex',
                  backgroundColor: 'rgb(37 99 235)',
                  color: 'rgb(255 255 255)'
                }}>시작시간
                </div>
                <div style={{
                  display: 'flex',
                }}>
                  <input type="time" value={serverBStartTime} onChange={e => setServerBStartTime(e.target.value)}/>
                </div>
                <div style={{
                  marginTop: '0.75rem',
                  display: 'flex',
                  backgroundColor: 'rgb(37 99 235)',
                  color: 'rgb(255 255 255)'
                }}>종료시간
                </div>
                <div style={{
                  display: 'flex',
                }}>
                  <input type="time" value={serverBEndTime} onChange={e => setServerBEndTime(e.target.value)}/>
                </div>

                <div style={{
                  marginTop: '0.75rem',
                  display: 'flex',
                  backgroundColor: 'rgb(37 99 235)',
                  color: 'rgb(255 255 255)'
                }}> 시작일자
                </div>
                <input type="date" value={serverBStartDate} onChange={e => setServerBStartDate(e.target.value)}/>
                <div style={{
                  marginTop: '0.75rem',
                  display: 'flex',
                  backgroundColor: 'rgb(37 99 235)',
                  color: 'rgb(255 255 255)'
                }}>끝일자
                </div>
                <input type="date" value={serverBEndDate} onChange={e => setServerBEndDate(e.target.value)}/>
              </div>


            </div>

        )
    }
  </div>;
}

export default SmartSearch;
