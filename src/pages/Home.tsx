import React, {useCallback, useEffect, useMemo, useRef, useState} from 'react'
import Row, {rowProps} from '../components/row/Row';
import InfiniteScroll from 'react-infinite-scroll-component';

// export interface rowData {
//   id: number;
//   projectName: string;
//   savedDate: string;E
// }

export interface jsonComment {
  postId: number;
  id: number;
  name: string;
  email: string;
  body: string;
}

export default function Home(): JSX.Element {
  const [rows, setRows] = useState<rowProps[]>([]);
  const [items, setItems] = useState<jsonComment[]>([]);
  const [noMore, setNoMore] = useState<boolean>(true);
  const [page, setPage] = useState<number>(2);

  const fetchComments = async () => {
    const res = await fetch(`http://localhost:300/comments?_page=${page}&_limit=20`)
    const data = res.json().then(funcData => funcData);
    return data;
  }

  const fetchData = async () => {
    const commentsServer = await fetchComments();
    setItems([...items, ...commentsServer]);

    if (commentsServer.length === 0 || commentsServer.length < 20) {
      setNoMore(false);
    }

    setPage(page + 1);
  }

  useEffect(() => {
    // const getComments = async () => {
    //     const res = await fetch('http://localhost:300/comments?_page=1&_limit=20')
    //     res.json().then(funcData=> setItems(funcData))
    // }
    //
    // // window.addEventListener('scroll', onScrollFn ) //컴포넌트 최초 렌더링 후 작동
    //
    // getComments();
    const arr: rowProps[] = []
    for (let i = 0; i < 100; i++) {
      arr.push({
        id: i.toString(),
        projectName: 'asdasdasdasdsadasdasdasdasdasdasdasdsadasdasdasdasdasdasdasdsadasdasdasdasdasdasdasdsadasdasdasdasdasdasdasdsadasdasdasd',
        savedDate: new Date().toLocaleString(),
      })
    }
    setRows(arr);

    return () => {
      // window.removeEventListener('scroll', onScrollFn);
    }
  }, []);

  return (
      <div className='relative  '>

        <div className=''>
          {/* column */}
          <div className='mt-5 bg-blue-100 flex  m-auto  w-[80%]  '>
            <div className='w-[10%] text-center '>NO</div>
            <div className='w-[70%]  text-center '>프로젝트명</div>
            <div className='w-[20%] text-center'>등록일자</div>
          </div>
          {/* row data*/}
          {/*<InfiniteScroll*/}
          {/*    dataLength={items.length} //This is important field to render the next data*/}
          {/*    next={fetchData}*/}
          {/*    hasMore={noMore}*/}
          {/*    loader={<h4>Loading...</h4>}*/}
          {/*    endMessage={*/}
          {/*        <p style={{textAlign: 'center'}}>*/}
          {/*            <b>Yay! You have seen it all</b>*/}
          {/*        </p>*/}
          {/*    }*/}
          {/*    // below props only if you need pull down functionality*/}
          {/*    refreshFunction={()=>{}}*/}
          {/*    pullDownToRefresh*/}
          {/*    pullDownToRefreshThreshold={50}*/}
          {/*    pullDownToRefreshContent={*/}
          {/*        <h3 style={{textAlign: 'center'}}>&#8595; Pull down to refresh</h3>*/}
          {/*    }*/}
          {/*    releaseToRefreshContent={*/}
          {/*        <h3 style={{textAlign: 'center'}}>&#8593; Release to refresh</h3>*/}
          {/*    }*/}
          {/*>*/}
          {
            rows.map((data, i) => {
              return <Row key={i} id={data.id}
                          projectName={data.projectName}
                          savedDate={data.savedDate}/>
            })
          }
          {/*</InfiniteScroll>*/}

        </div>
      </div>
  )
}
