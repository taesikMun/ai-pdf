import { useEffect } from "react";
import {  useLocation, useNavigate } from "react-router-dom";
import DocAddModal from "../components/modal/DocAddModal";
import DocRow from "../components/row/DocRow";

export default function ProjectDetail(): JSX.Element {

  const location = useLocation();
  const navigate = useNavigate();
  return (
    <div>
    <div className='flex mx-auto overflow-x-hidden justify-between'>
    <div className='w-[40%] mt-3 mr-2 ml-2'>
      <div className='flex justify-end '>
      
      </div>
      {/* BASE 문서 */}
      <div className='w-full  mt-2 flex justify-center bg-green-400'>
         BASE 문서
      </div>
      {/* column */}
      <div className =' text-sm bg-blue-100 flex  mt-2  w-full  '>
        <div className = 'w-[10%] text-center font-semibold'>NO</div>
        <div className = 'w-[70%] text-center font-semibold'>문서명</div>
        <div className = 'w-[20%] text-center font-semibold'>파일크기</div>
      </div>
      {/* row data */}
      <div className='overflow-y-scroll z-10'>
            <DocRow isInput={true} no={1} fileSize={131321231321312312313} key={1} docName='문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안'/>
            <DocRow isInput={true} no={1} fileSize={131321231321312312313} key={1} docName='문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안'/>
            <DocRow isInput={true} no={1} fileSize={131321231321312312313} key={1} docName='문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안'/>
            <DocRow isInput={true} no={1} fileSize={131321231321312312313} key={1} docName='문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안'/>
            <DocRow isInput={true} no={1} fileSize={131321231321312312313} key={1} docName='문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안'/>
            <DocRow isInput={true} no={1} fileSize={131321231321312312313} key={1} docName='문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안'/>
            <DocRow isInput={true} no={1} fileSize={131321231321312312313} key={1} docName='문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안'/>
            <DocRow isInput={true} no={1} fileSize={131321231321312312313} key={1} docName='문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안'/>
            <DocRow isInput={true} no={1} fileSize={131321231321312312313} key={1} docName='문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안'/>
            <DocRow isInput={true} no={1} fileSize={131321231321312312313} key={1} docName='문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안'/>
            <DocRow isInput={true} no={1} fileSize={131321231321312312313} key={1} docName='문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안'/>
            <DocRow isInput={true} no={1} fileSize={131321231321312312313} key={1} docName='문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안'/>
            <DocRow isInput={true} no={1} fileSize={131321231321312312313} key={1} docName='문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안'/>
            <DocRow isInput={true} no={1} fileSize={131321231321312312313} key={1} docName='문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안'/>
            <DocRow isInput={true} no={1} fileSize={131321231321312312313} key={1} docName='문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안'/>
            <DocRow isInput={true} no={1} fileSize={131321231321312312313} key={1} docName='문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안'/>
            <DocRow isInput={true} no={1} fileSize={131321231321312312313} key={1} docName='문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안'/>
            <DocRow isInput={true} no={1} fileSize={131321231321312312313} key={1} docName='문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안'/>
            <DocRow isInput={true} no={1} fileSize={131321231321312312313} key={1} docName='문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안'/>
            <DocRow isInput={true} no={1} fileSize={131321231321312312313} key={1} docName='문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안'/>
            <DocRow isInput={true} no={1} fileSize={131321231321312312313} key={1} docName='문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안'/>
            <DocRow isInput={true} no={1} fileSize={131321231321312312313} key={1} docName='문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안'/>
            <DocRow isInput={true} no={1} fileSize={131321231321312312313} key={1} docName='문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안'/>
            <DocRow isInput={true} no={1} fileSize={131321231321312312313} key={1} docName='문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안'/>
            <DocRow isInput={true} no={1} fileSize={131321231321312312313} key={1} docName='문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안'/>
            <DocRow isInput={true} no={1} fileSize={131321231321312312313} key={1} docName='문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안'/>
            <DocRow isInput={true} no={1} fileSize={131321231321312312313} key={1} docName='문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안'/>
            <DocRow isInput={true} no={1} fileSize={131321231321312312313} key={1} docName='문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안'/>
            <DocRow isInput={true} no={1} fileSize={131321231321312312313} key={1} docName='문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안'/>
            <DocRow isInput={true} no={1} fileSize={131321231321312312313} key={1} docName='문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안'/>
            <DocRow isInput={true} no={1} fileSize={131321231321312312313} key={1} docName='문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안'/>
            <DocRow isInput={true} no={1} fileSize={131321231321312312313} key={1} docName='문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안'/>
            <DocRow isInput={true} no={1} fileSize={131321231321312312313} key={1} docName='문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안'/>
            <DocRow isInput={true} no={1} fileSize={131321231321312312313} key={1} docName='문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안'/>
            <DocRow isInput={true} no={1} fileSize={131321231321312312313} key={1} docName='문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안'/>
            <DocRow isInput={true} no={1} fileSize={131321231321312312313} key={1} docName='문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안'/>
            <DocRow isInput={true} no={1} fileSize={131321231321312312313} key={1} docName='문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안'/>
            <DocRow isInput={true} no={1} fileSize={131321231321312312313} key={1} docName='문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안'/>
            <DocRow isInput={true} no={1} fileSize={131321231321312312313} key={1} docName='문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안'/>
            <DocRow isInput={true} no={1} fileSize={131321231321312312313} key={1} docName='문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안'/>
            <DocRow isInput={true} no={1} fileSize={131321231321312312313} key={1} docName='문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안'/>
            <DocRow isInput={true} no={1} fileSize={131321231321312312313} key={1} docName='문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안'/>
            <DocRow isInput={true} no={1} fileSize={131321231321312312313} key={1} docName='문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안'/>
            <DocRow isInput={true} no={1} fileSize={131321231321312312313} key={1} docName='문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안'/>
            <DocRow isInput={true} no={1} fileSize={131321231321312312313} key={1} docName='문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안'/>
            <DocRow isInput={true} no={1} fileSize={131321231321312312313} key={1} docName='문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안'/>
            <DocRow isInput={true} no={1} fileSize={131321231321312312313} key={1} docName='문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안'/>
            <DocRow isInput={true} no={1} fileSize={131321231321312312313} key={1} docName='문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안'/>
      </div>
            
      
    </div>

    <div className='w-[40%] mt-3 mr-4'>
      <div className='flex justify-end '>
        {/* <button className='w-52 relative bg-blue-600 rounded text-sm w-32 h-7 text-slate-100 '>
          <svg className="absolute w-6 h-6 mb-4 rounded" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" >
            <path stroke-linecap="round" stroke-linejoin="round" d="M12 4.5v15m7.5-7.5h-15" />
          </svg>
          <span className="font-semibold pl-7 text-sm ">
            새 문서 추가하기
          </span>
        </button> */}
        {/* <DocAddModal/> */}
      </div>
      {/* INPUT 문서 */}
        <div className='w-full mx-2 mt-2 flex justify-center bg-red-300'> 
        INPUT 문서
        </div>
          {/* column */}
        <div className ='text-sm bg-blue-100 flex  ml-2 mt-2  w-full  '>
          <div className = 'w-[10%] text-center font-semibold'>NO</div>
          <div className = 'w-[70%]  text-center font-semibold'>문서명</div>
          <div className = 'w-[20%] text-center font-semibold'>파일크기</div>
        </div>
          {/* row data */}
        <div className=" max-h-96 overflow-y-scroll ml-2 mt-2 w-full" >
            <DocRow isInput={true} no={1} fileSize={131321231321312312313} key={1} docName='문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안'/>
            <DocRow isInput={true} no={1} fileSize={131321231321312312313} key={1} docName='문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안'/>
            <DocRow isInput={true} no={1} fileSize={131321231321312312313} key={1} docName='문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안'/>
            <DocRow isInput={true} no={1} fileSize={131321231321312312313} key={1} docName='문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안'/>
            <DocRow isInput={true} no={1} fileSize={131321231321312312313} key={1} docName='문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안'/>
            <DocRow isInput={true} no={1} fileSize={131321231321312312313} key={1} docName='문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안'/>
            <DocRow isInput={true} no={1} fileSize={131321231321312312313} key={1} docName='문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안'/>
            <DocRow isInput={true} no={1} fileSize={131321231321312312313} key={1} docName='문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안'/>
            <DocRow isInput={true} no={1} fileSize={131321231321312312313} key={1} docName='문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안'/>
            <DocRow isInput={true} no={1} fileSize={131321231321312312313} key={1} docName='문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안'/>
            <DocRow isInput={true} no={1} fileSize={131321231321312312313} key={1} docName='문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안'/>
            <DocRow isInput={true} no={1} fileSize={131321231321312312313} key={1} docName='문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안'/>
            <DocRow isInput={true} no={1} fileSize={131321231321312312313} key={1} docName='문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안'/>
            <DocRow isInput={true} no={1} fileSize={131321231321312312313} key={1} docName='문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안'/>
            <DocRow isInput={true} no={1} fileSize={131321231321312312313} key={1} docName='문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안'/>
            <DocRow isInput={true} no={1} fileSize={131321231321312312313} key={1} docName='문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안'/>
            <DocRow isInput={true} no={1} fileSize={131321231321312312313} key={1} docName='문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안'/>
            <DocRow isInput={true} no={1} fileSize={131321231321312312313} key={1} docName='문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안'/>
            <DocRow isInput={true} no={1} fileSize={131321231321312312313} key={1} docName='문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안'/>
            <DocRow isInput={true} no={1} fileSize={131321231321312312313} key={1} docName='문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안'/>
            <DocRow isInput={true} no={1} fileSize={131321231321312312313} key={1} docName='문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안'/>
            <DocRow isInput={true} no={1} fileSize={131321231321312312313} key={1} docName='문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안'/>
            <DocRow isInput={true} no={1} fileSize={131321231321312312313} key={1} docName='문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안'/>
            <DocRow isInput={true} no={1} fileSize={131321231321312312313} key={1} docName='문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안'/>
            <DocRow isInput={true} no={1} fileSize={131321231321312312313} key={1} docName='문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안'/>
            <DocRow isInput={true} no={1} fileSize={131321231321312312313} key={1} docName='문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안'/>
            <DocRow isInput={true} no={1} fileSize={131321231321312312313} key={1} docName='문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안'/>
            <DocRow isInput={true} no={1} fileSize={131321231321312312313} key={1} docName='문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안'/>
            <DocRow isInput={true} no={1} fileSize={131321231321312312313} key={1} docName='문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안'/>
            <DocRow isInput={true} no={1} fileSize={131321231321312312313} key={1} docName='문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안'/>
            <DocRow isInput={true} no={1} fileSize={131321231321312312313} key={1} docName='문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안'/>
            <DocRow isInput={true} no={1} fileSize={131321231321312312313} key={1} docName='문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안'/>
            <DocRow isInput={true} no={1} fileSize={131321231321312312313} key={1} docName='문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안'/>
            <DocRow isInput={true} no={1} fileSize={131321231321312312313} key={1} docName='문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안'/>
            <DocRow isInput={true} no={1} fileSize={131321231321312312313} key={1} docName='문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안'/>
            <DocRow isInput={true} no={1} fileSize={131321231321312312313} key={1} docName='문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안'/>
            <DocRow isInput={true} no={1} fileSize={131321231321312312313} key={1} docName='문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안'/>
            <DocRow isInput={true} no={1} fileSize={131321231321312312313} key={1} docName='문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안'/>
            <DocRow isInput={true} no={1} fileSize={131321231321312312313} key={1} docName='문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안문서1_지능성_디지털맵구성방안'/>
          </div>
          
      </div>
      
    </div>
    {/*유사도 비교버튼  */}
    {/* <Link to={`/projects/comparison`}> */}
    
    {/* </Link>   */}
    </div>
  )
}
