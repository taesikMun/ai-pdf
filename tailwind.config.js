module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    extend: {},
    fontSize: {
      xs: '0.5rem',
      lg: '7rem',
      md: '4rem',
    }
  },
  plugins: [],
  // require("@tailwindcss/forms"),
}
